package edu.towson.cosc435.nsoesie.todoapp

import edu.towson.cosc431.nsoesie.todos.models.Todo
import java.util.*

interface ITodoRepository {
    fun getCount(): Int
    fun getTodo(idx: Int): Todo
    suspend fun fetchAll(): List<Todo>
    suspend fun removeTodo(todo: Todo)
    suspend fun update(idx: Int, todo: Todo)
     fun addTodo(todo: Todo)

}
//
//class MockDatastore : ITodoRepository {
//    private val todos = mutableListOf<Todo>()
//
//    init {
//        (0..10).forEach { i ->
//            todos.add(Todo(
//                content = "Content $i",
//                completed = false,
//                title = "Title $i"))
//        }
//    }
//
//    override fun fetchAll(): List<Todo> {
//        return todos
//    }
//
//    override fun getTodo(idx: Int): Todo {
//        return todos.get(idx)
//    }
//
//    override fun addTodo(todo: Todo) {
//        todos.add(0, todo)
//    }
//
//    override fun update(idx: Int, todo: Todo) {
//        todos.set(idx, todo)
//    }
//
//    override fun removeTodo(idx: Int) {
//        todos.removeAt(idx)
//    }
//
//    override fun getCount(): Int {
//        return todos.size
//    }
//}