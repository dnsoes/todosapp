package edu.towson.cosc435.nsoesie.todoapp

import android.content.Context
import androidx.room.Room
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import edu.towson.cosc431.nsoesie.todos.models.Todo
import edu.towson.cosc435.nsoesie.todoapp.database.TodoDB
import java.util.*

class Deserializer: ResponseDeserializable<Array<Todo>> {
    override fun deserialize(content: String): Array<Todo>? = Gson().fromJson(content, Array<Todo>::class.java)
}

class TodoRepository(context: Context) : ITodoRepository{

    private var todos: MutableList<Todo> = mutableListOf()
//    private val tododb:TodoDB;
    init {
//        val seed = (1..10).map { idx -> Todo("Todo${idx}", "Content${idx}", false, Date(), UUID.randomUUID()) }
//        todos.addAll(seed)

    val httpAsync = "https://my-json-server.typicode.com/rvalis-towson/todos_api/todos"
        .httpGet()
        .responseObject(Deserializer()) { _, _, result ->
            val (todo, err)= result;

            todo?.forEachIndexed { index, todo->
                todos.add(index,todo)
            }
        }

    httpAsync.join()
//        tododb = Room.databaseBuilder(
//            context,
//            TodoDB::class.java,
//            "todos.db"
//        ).allowMainThreadQueries()
//            .fallbackToDestructiveMigration()
//            .build()
//
//        if(tododb.todoDao().fetchAllTodos().size == 0){
//            (0..10).forEach { i ->
//                tododb.todoDao().addTodo(
//                    Todo(
//                        content = "Content $i",
//                        completed = false,
//                        title = "Title $i",
//                        id = UUID.randomUUID(),
//                        createdDate = Date()
//                    )
//                )
//            }
//        }
    }

     override fun addTodo(todo: Todo) {
        todos.add(todo)
    }

    override fun getCount(): Int {
        return todos.size
    }

     override fun getTodo (idx: Int): Todo {
        return todos.get(idx)
    }

    override suspend fun fetchAll(): List<Todo> {
        return todos
    }

    override suspend fun removeTodo(todo: Todo) {
        todos.remove(todo)
    }

    override suspend fun update(idx: Int, todo: Todo) {
        if(idx >= todos.size) throw Exception("Outside of bounds")
        todos[idx] = todo
    }

//
//    override  fun addTodo(todo: Todo) {
//    todos.add(todo)
//}
//
//    override fun getCount(): Int {
//        return todos.size
//    }
//
//    override fun getTodo (idx: Int): Todo {
//        return todos.get(idx)
//    }
//
//    override  fun fetchAll(): List<Todo> {
//        return todos
//    }
//
//    override  fun removeTodo(idx: Int) {
//        todos.removeAt(idx)
//    }
//
//    override  fun update(idx: Int, todo: Todo) {
//        if(idx >= todos.size) throw Exception("Outside of bounds")
//        todos[idx] = todo
//    }
}