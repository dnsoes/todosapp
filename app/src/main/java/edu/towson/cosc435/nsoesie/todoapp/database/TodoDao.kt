package edu.towson.cosc435.nsoesie.todoapp.database

import java.util.*
import androidx.room.*
import edu.towson.cosc431.nsoesie.todos.models.Todo


@Dao
interface TodoDao {
    @Insert
     fun addTodo(todo:Todo){

    }

    @Update
     fun updateTodo(todo: Todo){

    }

    @Delete
     fun deleteTodo(todo: Todo){

    }

    @Query("select * from todo")
     fun fetchAllTodos(): List<Todo>

//    @Query("select id, title, content, complete from Todos where id=  ${id}")
//    suspend fun fetchTodo(): Todo
}

class UUIDConverter {
    @TypeConverter
    fun fromString(uuid: String): UUID {
        return UUID.fromString(uuid);
    }

    @TypeConverter
    fun toString(uuid: UUID): String {
        return uuid.toString()
    }

}


@Database(entities = arrayOf(Todo::class), version = 1, exportSchema = false)
@TypeConverters(UUIDConverter::class)
abstract class TodoDB : RoomDatabase() {
    abstract fun todoDao(): TodoDao
}