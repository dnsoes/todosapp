package edu.towson.cosc435.nsoesie.todoapp

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_new_todo.*
import edu.towson.cosc431.nsoesie.todos.models.Todo
import java.util.*
import java.util.logging.Logger

class NewTodoActivity : AppCompatActivity() {

    companion object {

        val TODO = "todo"
    }

    private var todo: Todo? = null
    private var index = -1


    private fun createTodo() : Todo {
        return Todo(
            contents = text_edit_text.editableText.toString(),
            title = title_edit_text.editableText.toString(),
            completed = newtodo_completed_checkbox.isChecked,
            id = if(todo == null) { UUID.randomUUID() } else { todo!!.id },
            createdDate = if(todo == null) { Date() } else { todo!!.createdDate}
                )
    }

    private fun saveTodo() {
        val todo = createTodo()
        val intent = Intent();
        intent.putExtra(TODO, Gson().toJson(todo))
        Log.d("ACTIVITY LOG",todo.toString());
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_todo)
        save_btn.setOnClickListener { saveTodo() }

        if(intent.hasExtra(TODO)) {
            populateForm()
        }
    }


    private fun populateForm() {
        val json = intent.getStringExtra(TODO)
        index = intent.getIntExtra("Position", -1)
        todo = Gson().fromJson<Todo>(json, Todo::class.java)
        title_edit_text.editableText.clear()
        title_edit_text.editableText.append(todo?.title)
        text_edit_text.editableText.clear()
        text_edit_text.editableText.append(todo?.contents)
        newtodo_completed_checkbox.isChecked = if(todo == null) { false } else { todo!!.completed }
    }
}

