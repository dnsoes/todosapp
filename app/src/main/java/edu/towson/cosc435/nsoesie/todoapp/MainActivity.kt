package edu.towson.cosc435.nsoesie.todoapp

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.LinearLayout
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result;
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import edu.towson.cosc431.nsoesie.todos.models.Todo
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.OkHttpClient

class  MainActivity : AppCompatActivity(), TodoController {
    override lateinit var instance: ITodoRepository;
    /* To change initializer of created properties use File | Settings | File Templates. */

    override fun deleteTodo(idx: Int) {
       // instance.removeTodo(idx)
        recyclerView.adapter?.notifyItemRemoved(idx)    }

    override fun toggleCompleted(idx: Int) {
        val todo = instance.getTodo(idx)
        todo.completed = !todo.completed
       // instance.update(idx, todo)
        recyclerView.adapter?.notifyItemChanged(idx)    }

    override fun getTodo(idx: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        instance = TodoRepository(this);

        add_todo_btn.setOnClickListener { initCreateTodo() }
        recyclerView.adapter = TodoAdapter(this)
        recyclerView.layoutManager = LinearLayoutManager(this);



    }

    private fun initCreateTodo() {
        val intent = Intent(this, NewTodoActivity::class.java)

        startActivityForResult(intent, REQUEST_CODE)
    }

        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode) {
            REQUEST_CODE -> {
                when(resultCode) {
                    Activity.RESULT_OK -> {
                        val todo = Gson().fromJson<Todo>(data?.getStringExtra(NewTodoActivity.TODO), Todo::class.java)
                        recyclerView.scrollToPosition(0)
//                        instance.addTodo(todo)
                        recyclerView.scrollToPosition(0)
                        recyclerView.adapter?.notifyItemInserted(0)
                        // Log.d("ACTIVITY_LOG:", todo.toString())
                    }
                }
            }
        }
    }

    companion object {
        val REQUEST_CODE = 1
    }
}
