package edu.towson.cosc435.nsoesie.todoapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_new_todo.view.*
import kotlinx.android.synthetic.main.todo_card.view.*

class TodoAdapter(private val controller: TodoController) : RecyclerView.Adapter<TodoViewHolder>() {
    override fun onBindViewHolder(holder: TodoViewHolder, position: Int) {
        val name = holder.itemView.name
        val todo = controller.instance.getTodo(position);
        holder.itemView.name.text = todo.title
        holder.itemView.description.text = todo.contents
        holder.itemView.checkbox.isChecked = todo.completed
    }

    override fun getItemCount(): Int {
        return controller.instance.getCount()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.activity_todo_view, parent, false)
        val holder = TodoViewHolder(view)
        view.checkbox.setOnClickListener {
            val position = holder.adapterPosition
            controller.toggleCompleted(position)
        }


        view.setOnClickListener {
            controller.deleteTodo(holder.adapterPosition)
        }

        return holder;
    }

}

class TodoViewHolder(view: View) : RecyclerView.ViewHolder(view) {

}