package edu.towson.cosc435.nsoesie.todoapp


interface TodoController {
    val instance: ITodoRepository
    fun deleteTodo(idx: Int)
    fun toggleCompleted(idx: Int)
    fun getTodo(idx: Int)
}