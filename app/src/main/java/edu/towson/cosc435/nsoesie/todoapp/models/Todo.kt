package edu.towson.cosc431.nsoesie.todos.models

import android.os.Parcelable
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.time.LocalDateTime
import java.util.*

data class Todo(
        var title: String,
        var contents: String,
        var completed: Boolean,
        var createdDate: Date,
        @PrimaryKey
        var id: UUID
        ) {

        override fun toString():String{
              return "Title: ${this.title} \nContent: ${this.contents} \nCompleted: ${this.completed}"
        }
}